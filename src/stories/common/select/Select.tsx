import clsx from 'clsx';
import React from 'react';

import SelectSy from './select.module.css';

interface SelectProps {
  /**
   * Is this the principal call to action on the page?
   */
  primary?: boolean;
  /**
   * How large should the button be?
   */
  list: { value: string, displayNm: string }[];
  /**
   * label
   */
  label: string;
  /**
   * onSelectChange handler
   */
  onSelectChange?: () => void;
}

/**
 * Primary UI component for user interaction
 */
export const Select = () => {
  return (
    <>
      <label htmlFor="large" className="block mb-2 text-base font-medium text-gray-900 dark:text-white">Large select</label>
      <select onChange={(e) => console.log(e, '??E')} id="large" className="block w-full px-4 py-3 text-base text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
        <option selected>Choose a country</option>
        <option value="US">United States</option>
        <option value="CA">Canada</option>
        <option value="FR">France</option>
        <option value="DE">Germany</option>
      </select>
    </>
  );
};
