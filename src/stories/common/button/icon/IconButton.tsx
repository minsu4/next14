import clsx from 'clsx';
import React from 'react';

import ButtonSy from './iconButton.module.css';

interface IconButtonProps {
  /**
   * Is this the principal call to action on the page?
   */
  primary?: boolean;
  /**
   * How large should the button be?
   */
  size?: 'small' | 'medium' | 'large';
  /**
   * Button contents
   */
  label: string;
  /**
   * Optional click handler
   */
  onClick?: () => void;
}

/**
 * Primary UI component for user interaction
 */
export const IconButton = ({
  primary = false,
  size = 'medium',
  label,
  ...props
}: IconButtonProps) => {
  const mode = primary ? 'storybook-button--primary' : 'storybook-button--secondary';
  return (
    <>     
     <button
      type="button"
      className={clsx( `bg-blue-500 hover:bg-blue-700 text-white font-black rounded focus:outline-none  focus:ring-blue-500/0 focus:ring-opacity-100 transition-transform duration-300 ease-in-out transform shadow-md ${ButtonSy.btn}`, {
        "py-1 px-2 text-sm focus:ring-1": size == "small",
        "py-2 px-3 text-base focus:ring-2": size == "medium",
        "py-3 px-4 text-lg focus:ring-4": size == "large",
      })}
     
      {...props}
    >
      {label}
    </button>
    </>
  );
};
